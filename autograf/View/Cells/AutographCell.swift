//
//  AutographCell.swift
//  autograf
//
//  Created by DevMober on 7.3.21..
//

import UIKit

class AutographCell: UITableViewCell {

    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var ivAutographs: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    
    public var getAutograf: ((_ cell: AutographCell ) -> ())? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func onBuy(_ sender: Any) {
        if getAutograf != nil {
            getAutograf!(self)
        }
    }
    
}
