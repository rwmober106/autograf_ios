//
//  AthleteCell.swift
//  autograf
//
//  Created by DevMober on 5.3.21..
//

import UIKit

class AthleteCell: UITableViewCell {

    @IBOutlet weak var ivAthlete: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ivAthlete.clipsToBounds = true
        ivAthlete.layer.cornerRadius = ivAthlete.bounds.height / 2
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
