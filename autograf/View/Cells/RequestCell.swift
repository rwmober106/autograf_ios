//
//  RequestCell.swift
//  autograf
//
//  Created by DevMober on 9.3.21..
//

import UIKit

class RequestCell: UITableViewCell {

    @IBOutlet weak var ivAvatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblWAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        ivAvatar.clipsToBounds = true
        ivAvatar.layer.cornerRadius = ivAvatar.bounds.height / 2
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
