//
//  MyCollectionCell.swift
//  autograf
//
//  Created by DevMober on 18.3.21..
//

import UIKit

class MyCollectionCell: UICollectionViewCell {

    @IBOutlet weak var ivMyCollectionItem: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
