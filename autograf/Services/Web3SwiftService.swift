//
//  DigigrafContract.swift
//  autograf
//
//  Created by DevMober on 2.4.21..
//



import Foundation
import web3swift
import BigInt

class Web3swiftService {
    
    static let localStorage = LocalDatabase()
    static let keyservice = KeysService()
    
    static var web3instance: web3 {
        let web3 = try! Web3.new(URL(string: "https://ropsten.infura.io/v3/\(INFURAAPIKEY)")!)
        web3.addKeystoreManager(self.keyservice.keystoreManager()!)
        return web3
    }

    static var currentAddress: EthereumAddress? {
        let wallet = self.keyservice.selectedWallet()
        guard let address = wallet?.address else { return nil }
        let ethAddressFrom = EthereumAddress(address)
        return ethAddressFrom
    }
    
    static var currentAddressString: String? {
        let wallet = self.keyservice.selectedWallet()
        guard let address = wallet?.address else { return nil }
        return address
    }
    
    static var ropstenEtherscanExplorerString: String {
        return "https://ropsten.etherscan.io/tx/"
    }

//    static var digigrafContract: web3.web3contract? {
//        let contract = try! Web3swiftService.web3instance.contract(digigrafABI, at: ethContractAddress)
//        return contract
//    }


    func isEthAddressValid(address: String)->Bool{
        if EthereumAddress(address) != nil{
            return true
        }
        return false
    }

    // MARK: - Mint NFT
    
    func mintNFT(tokenURI: String, price: String, requestId: String, password: String?, completion: @escaping (TransactionSendingResult?) -> Void) {
        DispatchQueue.global().async {
            
            let web3 = Web3swiftService.web3instance
            
            guard let contractAddressLocal = ethContractAddress else {return}
            guard let amount = Web3.Utils.parseToBigUInt("0", units: .eth) else {return}
            guard let selectedKey = KeysService().selectedWallet()?.address else {return}
            guard let ethAddressFrom = EthereumAddress(selectedKey) else {return}
            
            var options = TransactionOptions()
            options.nonce = .pending
            options.callOnBlock = .pending
            options.gasLimit = .automatic
            options.gasPrice = .automatic
            options.from = ethAddressFrom
            options.value = amount
            
            guard let contract = web3.contract(digigrafABI, at: contractAddressLocal, abiVersion: 2) else {return}
            
            let params: [AnyObject] = [tokenURI, price, requestId] as [AnyObject]
            guard let writeTX = contract.write("mint",
                                               parameters: params,
                                               transactionOptions: options) else {return}
            
            let result = password == nil ?
                try? writeTX.send() :
                    try? writeTX.send(password: password!, transactionOptions: options)
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }

    // MARK: - Get Autograph Count

    func getAutographCount(completion: @escaping ([String: Any]) -> Void) {        

        DispatchQueue.global().async {

            let web3 = Web3swiftService.web3instance
            let ethAddressFrom = Web3swiftService.currentAddress
            
            var options = TransactionOptions.defaultOptions
            options.from = ethAddressFrom
            options.to = ethContractAddress
            options.value = 0 // or any other value you want to send

            guard let contract = web3.contract(digigrafABI, at: ethContractAddress, abiVersion: 2) else {return}
            options.gasPrice = TransactionOptions.GasPricePolicy.automatic
            options.gasLimit = TransactionOptions.GasLimitPolicy.automatic
            contract.transactionOptions = options
            
            guard let transaction = contract.read("autographCount", transactionOptions: options) else {return}
            let result = try? transaction.call(transactionOptions: options)
            
            DispatchQueue.main.async {
                completion(result ?? [:])
            }
        }
    }
    
    // MARK: - Get Autograph Details
    
    func getAutographById(id: Int, completion: @escaping ([String: Any]) -> Void) {
        
        DispatchQueue.global().async {

            let web3 = Web3swiftService.web3instance
            let ethAddressFrom = Web3swiftService.currentAddress

            var options = TransactionOptions.defaultOptions
            options.from = ethAddressFrom
            options.to = ethContractAddress
            options.value = 0 // or any other value you want to send

            guard let contract = web3.contract(digigrafABI, at: ethContractAddress, abiVersion: 2) else {return}
            options.gasPrice = TransactionOptions.GasPricePolicy.automatic
            options.gasLimit = TransactionOptions.GasLimitPolicy.automatic
            contract.transactionOptions = options
            
            let params: [AnyObject] = [id] as [AnyObject]
            guard let transaction = contract.read("autographs", parameters: params, transactionOptions: options) else {return}
            let result = try? transaction.call(transactionOptions: options)
            
            DispatchQueue.main.async {
                completion(result ?? [:])
            }
        }
    }
    
    func getContractOwner(_ completion: @escaping ([String: Any]) -> Void) {
        
        DispatchQueue.global().async {

            let web3 = Web3swiftService.web3instance
            let ethAddressFrom = Web3swiftService.currentAddress

            var options = TransactionOptions.defaultOptions
            options.from = ethAddressFrom
            options.to = ethContractAddress
            options.value = 0 // or any other value you want to send

            guard let contract = web3.contract(digigrafABI, at: ethContractAddress, abiVersion: 2) else {return}
            options.gasPrice = TransactionOptions.GasPricePolicy.automatic
            options.gasLimit = TransactionOptions.GasLimitPolicy.automatic
            contract.transactionOptions = options
            
            guard let transaction = contract.read("_contractOwner", transactionOptions: options) else {return}
            let result = try? transaction.call(transactionOptions: options)
            
            DispatchQueue.main.async {
                completion(result ?? [:])
            }
        }
    }

    // MARK: Buy NFT
    
    func buyNFT(tokenId: Int, price: String, password: String?, completion: @escaping (TransactionSendingResult?) -> Void) {
        DispatchQueue.global().async {
            
            let web3 = Web3swiftService.web3instance
            
            guard let contractAddressLocal = ethContractAddress else {return}
            guard let amount = Web3.Utils.parseToBigUInt(price, units: .eth) else {return}
            guard let selectedKey = KeysService().selectedWallet()?.address else {return}
            guard let ethAddressFrom = EthereumAddress(selectedKey) else {return}
            
            var options = TransactionOptions()
            options.nonce = .pending
            options.callOnBlock = .pending
            options.gasLimit = .automatic
            options.gasPrice = .automatic
            options.from = ethAddressFrom
            options.value = amount
            
            guard let contract = web3.contract(digigrafABI, at: contractAddressLocal, abiVersion: 2) else {return}
            
            let params: [AnyObject] = [tokenId] as [AnyObject]
            guard let writeTX = contract.write("buyFromOwner",
                                               parameters: params,
                                               transactionOptions: options) else {return}
            
            let result = password == nil ?
                try? writeTX.send() :
                    try? writeTX.send(password: password!, transactionOptions: options)
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func setPrice(tokenId: Int, price: String, password: String?, completion: @escaping (TransactionSendingResult?) -> Void) {
        DispatchQueue.global().async {
            
            let web3 = Web3swiftService.web3instance
            
            guard let contractAddressLocal = ethContractAddress else {return}
            guard let amount = Web3.Utils.parseToBigUInt("0", units: .eth) else {return}
            guard let selectedKey = KeysService().selectedWallet()?.address else {return}
            guard let ethAddressFrom = EthereumAddress(selectedKey) else {return}
            
            var options = TransactionOptions()
            options.nonce = .pending
            options.callOnBlock = .pending
            options.gasLimit = .automatic
            options.gasPrice = .automatic
            options.from = ethAddressFrom
            options.value = amount
            
            guard let contract = web3.contract(digigrafABI, at: contractAddressLocal, abiVersion: 2) else {return}
            
            let params: [AnyObject] = [tokenId, price] as [AnyObject]
            guard let writeTX = contract.write("setPrice",
                                               parameters: params,
                                               transactionOptions: options) else {return}
            
            let result = password == nil ?
                try? writeTX.send() :
                    try? writeTX.send(password: password!, transactionOptions: options)
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }


    //MARK: - Get untrusted address
    func getUntrustedAddress(completion: @escaping (String?) -> Void) {
        DispatchQueue.global().async {
            let wallet = Web3swiftService.keyservice.localStorage.getWallet()
            guard let address = wallet?.address else {
                completion(nil)
                return
            }
            completion(address)
        }
    }

    // MARK: - Get ETH balance
    func getETHbalance(completion: @escaping (BigUInt?, Error?) -> Void) {
            let wallet = Web3swiftService.keyservice.localStorage.getWallet()
        DispatchQueue.global().async {

            guard let address = wallet?.address else {
                return
            }
            let ETHaddress = EthereumAddress(address)
            let web3Main = Web3swiftService.web3instance
            do {
                let balanceResult = try web3Main.eth.getBalance(address: ETHaddress!)

                completion(balanceResult, nil)
            }
            catch{
                return
            }

        }
    }



}

enum Result<T> {
    case Success(T)
    case Error(Error)
}
