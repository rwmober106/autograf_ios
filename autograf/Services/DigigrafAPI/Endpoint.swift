//
//  Endpoint.swift
//  ThatDubaiGirl
//
//  Created by DevMober on 23.11.20..
//

import Foundation

enum Endpoint: String {    
    case login = "login"
    case register = "register"
    case editaccount = "editaccount"
    case getaccount = "getaccount"
    case accept = "accept"
    case customer = "customer"
}
