//
//  AutografAPI.swift
//  Autograf
//
//  Created by DevMober on 7.3.21..
//

import Foundation
import Alamofire
import ObjectMapper


class DigigrafAPI {
    
    static let shared = DigigrafAPI()
    public static let mainURL = "http://206.81.10.199/api/v1/"
    let celebrityURL = "http://206.81.10.199/api/v1/celebrity/"
    let customerURL = "http://206.81.10.199/api/v1/customer/"
    let requestURL = "http://206.81.10.199/api/v1/request/"
    let autographURL = "http://206.81.10.199/api/v1/autograph"
    public static let avatarImageURL = "http://206.81.10.199/images/avatar/"
    public static let nftImageURL = "http://206.81.10.199/images/nft/"
    
    // Celebrity API
    
    func celebrityLogin(_ email: String, _ password: String, _ callback: @escaping (Bool, Celebrity?, String?) -> Void) {
        
        let urlString = celebrityURL + Endpoint.login.rawValue
        let params = [
            "email": email,
            "password": password
        ]
                
        AF.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(false, nil, msg)                    
                } else {
                    if let data = jsonObject["data"] as? [String: Any] {
                        guard let user = Mapper<Celebrity>().map(JSON: data) else {
                            callback(false, nil, "No objects found")
                            return
                        }
                        callback(true, user, msg)
                    } else {
                        callback(false, nil, msg)
                    }
                }
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }
        
    }
    
    func celebrityUpdate(firstName: String, lastName: String, email: String, phoneNumber: String, celebrityType: String, cAvatarUrl: String, walletAddress: String, celebrityId: String, _ callback: @escaping (Bool, Celebrity?, String?) -> Void) {

        let urlString = celebrityURL + Endpoint.editaccount.rawValue + "/" + celebrityId
        let params = [
            "first_name": firstName,
            "last_name": lastName,
            "email": email,
            "phone_number": phoneNumber,
            "celebrity_type": celebrityType,
            "cavatar_url": cAvatarUrl,
            "cwallet_address": walletAddress
        ]

        AF.request(urlString, method: .put, parameters: params, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }

                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(false, nil, msg)
                } else {
                    if let data = jsonObject["data"] as? [String: Any] {
                        guard let user = Mapper<Celebrity>().map(JSON: data) else {
                            callback(false, nil, "No objects found")
                            return
                        }
                        callback(true, user, msg)
                    } else {
                        callback(false, nil, msg)
                    }
                }

            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }

    }
    
    func getRequests(_ celebrityId: String, _ callback: @escaping (Bool, [Request]?, String?) -> Void) {
        
        let urlString = requestURL
        let params = [
            "celebrity_id": celebrityId
        ]
                
        AF.request(urlString, method: .get, parameters: params, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(false, nil, msg)
                } else {
                    if let data = jsonObject["data"] as? [[String: Any]] {
                        let objects = Mapper<Request>().mapArray(JSONArray: data)
                        callback(true, objects, msg)
                    } else {
                        callback(false, nil, msg)
                    }
                }
                
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }
    }
    
    func uploadAutograph(_ celebrityId: String, _ autographName: String, _ autographDesc: String, _ base64String: String, _ price: String, _ callback: @escaping (Bool, String?, String?) -> Void) {
        
        let urlString = autographURL
        let params = [
            "celebrity_id": celebrityId,
            "autograph_name": autographName,
            "autograph_desc": autographDesc,
            "base64image": base64String,
            "price": price
        ]
                
        AF.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(false, nil, msg)
                } else {
                    if let data = jsonObject["data"] as? String {                        
                        callback(true, data, msg)
                    } else {
                        callback(false, nil, msg)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
//                print("Error: Trying to convert JSON data to string")
                return
            }
        }
    }
    
//    func uploadAutograph(_ celebrityId: String, _ autographName: String, _ autographDesc: String, _ imageData: Data?, _ price: String, _ callback: @escaping (Bool, String?, String?) -> Void) {
//
//            let urlString = autographURL
//            let params = [
//                "celebrity_id": celebrityId,
//                "autograph_name": autographName,
//                "autograph_desc": autographDesc,
//                "price": price
//            ]
//
//            AF.upload(multipartFormData: { (multipartFormData) in
//                for (key, value) in params {
//                    if let temp = value as? String {
//                        multipartFormData.append(temp.data(using: .utf8)!, withName: key)
//                    }
//                }
//                if let data = imageData {
//                    multipartFormData.append(data, withName: "file", fileName: "\(Date.init().timeIntervalSince1970).png", mimeType: "image/png")
//                }
//            }, to: urlString, method: .post, headers: nil)
//            .responseJSON { (AFdata) in
//                do {
//                    guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
//                        print("Error: Cannot convert data to JSON object")
//                        return
//                    }
//                    let msg = jsonObject["message"] as? String
//                    let status = jsonObject["error"] as! Bool
//                    if status {
//                        callback(false, nil, msg)
//                    } else {
//                        if let data = jsonObject["data"] as? [String: Any] {
//
//                            callback(true, msg, msg)
//                        } else {
//                            callback(false, nil, msg)
//                        }
//                    }
//                } catch {
//                    print("Error: Trying to convert JSON data to string")
//                    return
//                }
//            }
//
//        }


    func handleRequest(requestId: String, status: Int, _ callback: @escaping (Bool, String?) -> Void) {
        
        let urlString = requestURL + Endpoint.accept.rawValue
        let params = [
            "request_id": requestId,
            "status": status
        ] as [String : Any]
        AF.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(false, msg)
                } else {
                    callback(true, msg)
                }
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }
        
    }

    
    // Customer API
    
    func customerRegister(_ walletAddress: String, _ customerName: String, _ base64String: String, _ callback: @escaping (Bool, Customer?, String?) -> Void) {
        
        let urlString = customerURL + Endpoint.register.rawValue
        let params = [
            "wallet_address": walletAddress,
            "customer_name": customerName,
            "base64image": base64String
        ]
                
        AF.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(false, nil, msg)
                } else {
                    if let data = jsonObject["data"] as? [String: Any] {
                        guard let user = Mapper<Customer>().map(JSON: data) else {
                            callback(false, nil, "No objects found")
                            return
                        }
                        callback(true, user, msg)
                    } else {
                        callback(false, nil, msg)
                    }
                }
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }
    }
    
    func getCelebrities(_ callback: @escaping (Bool, [Celebrity]?, String?) -> Void) {
        
        let urlString = celebrityURL
                
        AF.request(urlString, method: .get, parameters: nil, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(false, nil, msg)
                } else {
                    if let data = jsonObject["data"] as? [[String: Any]] {
                        let objects = Mapper<Celebrity>().mapArray(JSONArray: data)
                        callback(true, objects, msg)
                    } else {
                        callback(false, nil, msg)
                    }
                }
                
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }
    }
    
    func getAccount(_ customerId: String, _ callback: @escaping (Bool, Customer?, String?) -> Void) {
        
        let urlString = customerURL + Endpoint.getaccount.rawValue 
        let params = [
            "customer_id": customerId
        ]
                
        AF.request(urlString, method: .get, parameters: params, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(false, nil, msg)
                } else {
                    if let data = jsonObject["data"] as? [String: Any] {
                        guard let user = Mapper<Customer>().map(JSON: data) else {
                            callback(false, nil, "No objects found")
                            return
                        }
                        callback(true, user, msg)
                    } else {
                        callback(false, nil, msg)
                    }
                }
                
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }
    }
    
    func customerUpdate(walletAddress: String, customerName: String, base64Image: String, customerId: String, _ callback: @escaping (Bool, Customer?, String?) -> Void) {

        let urlString = customerURL + Endpoint.editaccount.rawValue + "/" + customerId
        let params = [
            "wallet_address": walletAddress,
            "customer_name": customerName,
            "base64image": base64Image
        ]

        AF.request(urlString, method: .put, parameters: params, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(false, nil, msg)
                } else {
                    if let data = jsonObject["data"] as? [String: Any] {
                        guard let user = Mapper<Customer>().map(JSON: data) else {
                            callback(false, nil, "No objects found")
                            return
                        }
                        callback(true, user, msg)
                    } else {
                        callback(false, nil, msg)
                    }
                }

            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }

    }
    
    func makeRequest(_ customerId: String, _ celebrityId: String, _ base64String: String, _ message: String, _ callback: @escaping (Bool, String?) -> Void) {
        
        let urlString = requestURL
        let params = [
            "customer_id": customerId,
            "celebrity_id": celebrityId,
            "base64image": base64String,
            "message": message
        ]
        AF.request(urlString, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(true, msg)
                } else {
                    callback(false, msg)
                }
                
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }
        
    }
    
    func getMyRequests(_ customerId: String, _ callback: @escaping (Bool, [Request]?, String?) -> Void) {
        
        let urlString = requestURL + Endpoint.customer.rawValue
        let params = [
            "customer_id": customerId
        ]
                
        AF.request(urlString, method: .get, parameters: params, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                let msg = jsonObject["message"] as? String
                let status = jsonObject["error"] as! Bool
                if status {
                    callback(false, nil, msg)
                } else {
                    if let data = jsonObject["data"] as? [[String: Any]] {
                        let user = Mapper<Request>().mapArray(JSONArray: data) 
                        callback(true, user, msg)
                    } else {
                        callback(false, nil, msg)
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
//                print("Error: Trying to convert JSON data to string")
                return
            }
        }
    }
    
    

    // MARK: Get data from metadata json
    
    func getJsonData(metadatahash: String, _ callback: @escaping (Metadata?) -> Void) {

        let urlString = ipfsGateway + metadatahash

        AF.request(urlString, method: .get, parameters: nil, headers: nil).validate(statusCode: 200 ..< 299).responseJSON { AFdata in
            do {
                guard let jsonObject = try JSONSerialization.jsonObject(with: AFdata.data!) as? [String: Any] else {
                    print("Error: Cannot convert data to JSON object")
                    return
                }
                guard let metadata = Mapper<Metadata>().map(JSON: jsonObject) else {
                    return
                }
                callback(metadata)
            } catch {
                print("Error: Trying to convert JSON data to string")
                return
            }
        }

    }

    
    
}

