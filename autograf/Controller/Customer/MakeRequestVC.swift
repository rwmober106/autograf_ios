//
//  MakeRequestVC.swift
//  autograf
//
//  Created by DevMober on 8.3.21..
//

import UIKit

class MakeRequestVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var ivRequest: UIImageView!
    @IBOutlet weak var btnAddPhoto: UIButton!
    @IBOutlet weak var tvMessage: UITextView!
    @IBOutlet weak var btnRequest: UIButton!
    
    private var newSizedImage: UIImage?
    let web3SwiftService = Web3swiftService()
    private var myWalletAddress: String?
    private var base64encodeImage: String?
    
    public var celebrityId: Int?
    public var imageUrl: String?
    public var message: String?
    
    public var myRequestsViewController: MyRequestsVC?
        
    override func viewDidLoad() {
        super.viewDidLoad()

        btnAddPhoto.layer.borderWidth = 1
        btnAddPhoto.layer.borderColor = UIColor.white.cgColor
        btnAddPhoto.clipsToBounds = true
        btnAddPhoto.layer.cornerRadius = 10
        
        if (myRequestsViewController != nil) {
            ivRequest.sd_setImage(with: URL(string: String(format: "%@%@", DigigrafAPI.nftImageURL, imageUrl!)), completed: nil)
            tvMessage.text = message
            btnRequest.isEnabled = false
            btnRequest.alpha = 0.5
        }
        
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onRequest(_ sender: Any) {
        
        guard let message = tvMessage.text, message.isEmpty == false else {
            return UIManager.shared.showAlert(vc: self, title: "", message: "You must add your message")
        }
        
        web3SwiftService.getUntrustedAddress(completion: { (address) in
            if address != nil {
                self.myWalletAddress = address
            }
        })
        
        if (self.newSizedImage != nil) {
            let imageData = self.newSizedImage!.jpegData(compressionQuality: 1.0)
            base64encodeImage = imageData?.base64EncodedString(options: .lineLength64Characters)
        }
        
        if let customerId = UserDefaults.standard.value(forKey: "customerId") {
            let cId: String = String(celebrityId!)
            
            UIManager.shared.showHUD(view: self.view)
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
            DigigrafAPI.shared.makeRequest(customerId as! String, cId, base64encodeImage!, tvMessage.text) { (success, msg) in
                UIManager.shared.hideHUD()
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                if success {
                    UIManager.shared.showAlert(vc: self, title: "", message: msg!)
                } else {
                    UIManager.shared.showAlert(vc: self, title: "", message: msg!)
                }
            }
        }        
        
    }
    
    
    @IBAction func onAddPhoto(_ sender: Any) {
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {

        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {

            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK:- UIImagePickerViewDelegate.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        self.dismiss(animated: true) { [weak self] in

            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            
            let newSize = image.size * 0.5
            self!.newSizedImage = image.resizeUI(size: newSize)
            self?.ivRequest.image = self!.newSizedImage
            self?.ivRequest.contentMode = .scaleAspectFit
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

}

