//
//  CustomerAccountSettingVC.swift
//  autograf
//
//  Created by DevMober on 29.3.21..
//

import UIKit

class CustomerAccountSettingVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var ivPhoto: UIImageView!
    @IBOutlet weak var tfName: UITextField!
    
    let service = Web3swiftService()
    let keysService = KeysService()
    let localDatabase = LocalDatabase()
    
    private var myAddress: String?
    public var customer: Customer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
    }
    
    func loadData() {
        if let name = customer?.customerName {
            self.tfName.text = name
        }
        if let avatar = customer?.avatarUrl, !avatar.isEmpty {
            self.ivPhoto.sd_setImage(with: URL(string: String(format: "%@%@", DigigrafAPI.avatarImageURL, avatar)), completed: nil)
        }
        myAddress = customer?.walletAddress
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onEditPhoto(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onSaveEditing(_ sender: Any) {
        
        guard let name = self.tfName.text, !name.isEmpty else {
            return UIManager.shared.showAlert(vc: self, title: "", message: "You must input your name")
        }
        
        let imageData = self.ivPhoto.image!.jpegData(compressionQuality: 1.0)
        let base64encodeImage: String = (imageData?.base64EncodedString(options: .lineLength64Characters))!
        let customerId: String = String((customer?.customerId)!)
        let customerName = self.tfName.text
        UIManager.shared.showHUD(view: self.view)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        if let walletAddress = customer?.walletAddress {
            DigigrafAPI.shared.customerUpdate(walletAddress: walletAddress, customerName: customerName!, base64Image: base64encodeImage, customerId: customerId) { (success, customer, msg) in
                UIManager.shared.hideHUD()
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                if success {
                    self.customer = customer
                    UIManager.shared.showAlert(vc: self, title: "", message: msg!)
                } else {
                    UIManager.shared.showAlert(vc: self, title: "Error", message: msg!)
                }
            }
        }
        
    }
    
    func privateKeyAlert(privateKey: String) {
        let alert = UIAlertController(title: "Private key", message: privateKey, preferredStyle: UIAlertController.Style.alert)
        
        let copyAction = UIAlertAction(title: "Copy", style: .default) { (_) in
            let pasteboard = UIPasteboard.general
            pasteboard.string = privateKey
        }
        
        let closeAction = UIAlertAction(title: "Close", style: .cancel) { (_) in
        }
        alert.addAction(copyAction)
        alert.addAction(closeAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func showPrivateKey(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Show private key", message: nil, preferredStyle: UIAlertController.Style.alert)
        
        alert.addTextField { (textField) in
            textField.isSecureTextEntry = true
            textField.placeholder = "Enter your password"
        }
        
        let enterPasswordAction = UIAlertAction(title: "Enter", style: .default) { (_) in
            let passwordTextField = alert.textFields![0] as UITextField
            if let privateKey = self.keysService.getWalletPrivateKey(password: passwordTextField.text!) {
                self.privateKeyAlert(privateKey: privateKey)
                
            } else {
                UIManager.shared.showAlert(vc: self, title: "", message: "Wrong Password")
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            
        }
        
        alert.addAction(enterPasswordAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
   
    @IBAction func logout(_ sender: UIButton) {
        let alert = UIAlertController(title: "Log out", message: "Are you sure you would like to log out? You will have to create a new wallet or import an existing one again.", preferredStyle: .alert)
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        let confirmAction = UIAlertAction(title: "Logout", style: .destructive) { _ in
            
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
        
            self.localDatabase.deleteWallet { (error) in
                if error == nil {
                    
                    guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                          let sceneDelegate = windowScene.delegate as? SceneDelegate
                    else {
                        return
                    }
                    sceneDelegate.goToRoot()
                } else {
                    UIManager.shared.showAlert(vc: self, title: "Error", message: error!.localizedDescription)
                }
            }
        }
        alert.addAction(cancel)
        alert.addAction(confirmAction)
        present(alert, animated: true)
    }
    
    @IBAction func addBalance(_ sender: Any) {
//        let alert = UIAlertController(title: "Add Balance to your Wallet?", message: "Clicking Ok will open a link to the Oasis Devnet Faucet in Safari and copy your wallet address to your pasteboard for signup.", preferredStyle: .alert)
//
//        let enterPasswordAction = UIAlertAction(title: "Ok", style: .default) { (_) in
//            let pasteboard = UIPasteboard.general
//            pasteboard.string = self.myAddress!
//            guard let url = URL(string: "https://faucet.oasiscloud.io/") else { return }
//            UIApplication.shared.open(url)
//        }
//        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
//
//        }
//
//        alert.addAction(enterPasswordAction)
//        alert.addAction(cancelAction)
//
//        self.present(alert, animated: true, completion: nil)
        
       
    }
    
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {

        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {

            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK:- UIImagePickerViewDelegate.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        self.dismiss(animated: true) { [weak self] in

            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            
            let newSize = CGSize(200.0, 200.0)
            let newSizedImage = image.resizeUI(size: newSize)
            self?.ivPhoto.image = newSizedImage
            self?.ivPhoto.contentMode = .scaleAspectFit
        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}
