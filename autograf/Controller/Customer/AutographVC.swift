//
//  AutographVC.swift
//  autograf
//
//  Created by DevMober on 19.3.21..
//

import UIKit
import web3swift
import SDWebImage
import BigInt
import ObjectMapper

class AutographVC: UIViewController {
    
    public var autograph: Autograph?
    public var metadata: Metadata?
    public var requestId: Int?

    @IBOutlet weak var btnPurchase: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblPriceValue: UILabel!
    @IBOutlet weak var tvCreator: UITextView!
    @IBOutlet weak var tvOwner: UITextView!
    @IBOutlet weak var ivAutograph: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    
    let service = Web3swiftService()
    private var currentBalance: String?
    private var walletAddress: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // gets ETH balance
        service.getETHbalance { (result, _) in
            DispatchQueue.main.async { [self] in
                currentBalance = Web3Utils.formatToEthereumUnits(result!, toUnits: .eth, decimals: 6, decimalSeparator: ".")
            }
        }
        
        setupUI()
        
        // loads your request NFT
        if let viewcontrollers = self.navigationController?.children {
            if viewcontrollers[0] is MyRequestsVC {
                loadBlockchainData()
            }
        }
        
        btnPurchase.addTarget(self, action: #selector(onBtnPurchase), for: .touchDown)
        
    }
    
    @objc func onBtnPurchase() {
        
        if let isFirstSold = autograph?.isFirstSold, isFirstSold == true {
            if let forSale = autograph?.forSale, forSale == false {
                let alertController = UIAlertController(title: nil, message: "Please set the price", preferredStyle: .alert)
                alertController.addTextField { (tfPrice) in
                    tfPrice.keyboardType = .decimalPad
                    tfPrice.placeholder = "0.2 ETH"
                }
                alertController.addTextField { textField in
                    textField.placeholder = "Password"
                    textField.isSecureTextEntry = true
                }
                let releaseAction = UIAlertAction(title: "Put it on the Market Place", style: .default) { [self] (action) in
                    
                    // checks if price is a valid value
                    guard let price = alertController.textFields![0].text, price.doubleValue != nil && (price.doubleValue)! > 0.001 else {
                        return UIManager.shared.showAlert(vc: self, title: "", message: "Please add right price")
                    }
                    let p = Double(price)
                    let price1 = String(p!)
                    
                    guard let password = alertController.textFields?[1] else { return }
                    
                    // calls setPrice function
                    UIManager.shared.showHUD(view: self.view)
                    let id = self.autograph?.id
                    
                    service.setPrice(tokenId: id!, price: price1, password: password.text!) { (results) in
                        UIManager.shared.hideHUD()
                        if results != nil {
                            UIManager.shared.showTx("Set Price Successfully", txhash: results!.hash, with: "To confirm check your transaction hash on Ropsten Etherscan Explorer: \n\(results!.hash)", for: self, dismissOnCompletion: false)
                            self.navigationController?.popViewController(animated: true)
                            
                        }
                        else{
                            UIManager.shared.showAlert(vc: self, title: "Failed", message: "Failed, please try again")
                        }
                    }
                    
                }
                alertController.addAction(releaseAction)
                self.present(alertController, animated: true, completion: nil)
            } else {
                
                // buys NFT
                buyNFT()
            }
            
        } else {
            
            // buys NFT
            buyNFT()
            
        }
        
    }
    
    func loadBlockchainData() {

        DispatchQueue.main.async {
            UIManager.shared.showHUD(view: self.view)
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }

        service.getAutographCount { [self] (result) in

            let count = Int(result["0"] as! BigUInt)
            if count > 0 {
                var j = 0
                for i in 1...count {
                    
                    service.getAutographById(id: i) { (result) in
                        j += 1
                        let id = Int(result["id"] as! BigUInt)

                        

                        guard var autograph = Mapper<Autograph>().map(JSON: result ) else {
                            return
                        }
                        autograph.id = id
                        
                        if let requestId = autograph.requestId, !requestId.isEmpty {
                            if self.requestId == Int(requestId) {
                                self.autograph = autograph
                                DigigrafAPI.shared.getJsonData(metadatahash: autograph.uri!) { (metadata) in
                                    self.metadata = metadata
                                    setupUI()
                                }
                            }
                        }
                        
                        if count == j {
                            DispatchQueue.main.async {
                                UIManager.shared.hideHUD()
                                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                            }
                        }

                    }

                }

            }
        }
    }
    
    func setupUI() {
        
        guard metadata != nil else {
            return
        }
        guard autograph != nil else {
            return
        }
        
        lblTitle.text = metadata?.name
        lblPriceValue.text = (autograph?.price)! + " ETH"
        lblDesc.text = metadata?.description
        tvCreator.text = "Creator: \n" + (autograph?.creator!.address)!
        tvOwner.text = "Owner: \n" + (autograph?.owner!.address)!
        ivAutograph.sd_setImage(with: URL(string: String(format: "%@", metadata!.image!)), completed: nil)
        
        
        if let isFirstSold = autograph?.isFirstSold, isFirstSold == true {
            lblPrice.text = "Sold For"
        }
        if let forSale = autograph?.forSale, !forSale {
            btnPurchase.setTitle("Buy", for: .normal)
            if let viewcontrollers = self.navigationController?.children {
                if viewcontrollers[0] is MyAccountVC {
                    btnPurchase.setTitle("Sell", for: .normal)
                }
            }
            
            if !(autograph?.requestId!.isEmpty)! {
                btnPurchase.setTitle("Buy", for: .normal)
            }
            
        } else {
            
            walletAddress = Web3swiftService.currentAddressString
            let owner = autograph?.owner
            if walletAddress == owner?.address {
                btnPurchase.isEnabled = false
                btnPurchase.alpha = 0.5
                
                let viewcontrollers = self.navigationController?.children
                if viewcontrollers![0] is MyAccountVC {
                    btnPurchase.setTitle("Sell", for: .normal)
                    
                }
            }
            
        }
    }
    
    private func buyNFT() {
        let priceDouble = Double((autograph?.price!)!)
        guard let balance = Double(currentBalance!), balance >= priceDouble! else {
            return UIManager.shared.showAlert(vc: self, title: "", message: "Your current balance is not enough to buy. Please add balance.")
        }
        
        UIManager.shared.showHUD(view: self.view)
        
        let alert = UIAlertController(title: "Buy Autograph", message: "Please enter your password to confirm.", preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = "Password"
            textField.isSecureTextEntry = true
        }
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in
            UIManager.shared.hideHUD()
        })
        let confirmAction = UIAlertAction(title: "OK", style: .default) { [self, weak alert] _ in
            guard let alertController = alert, let password = alertController.textFields?.first else { return }
            let id = autograph?.id
            let price = autograph?.price
            self.service.buyNFT(tokenId: id!, price: price!, password: password.text!, completion: {(results) in
                UIManager.shared.hideHUD()
                if results != nil {
                    UIManager.shared.showTx("Bought Successfully", txhash: results!.hash, with: "To confirm check your transaction hash on Ropsten Etherscan Explorer: \n\(results!.hash)", for: self, dismissOnCompletion: false)
                }
                else{
                    UIManager.shared.showAlert(vc: self, title: "Failed", message: "Failed, please try again")
                }
            })
        }
        alert.addAction(cancel)
        alert.addAction(confirmAction)

        present(alert, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func openEtherscan (_ sender: Any) {
        let id: String = String((autograph?.id)!)
        let urlString = "https://ropsten.etherscan.io/token/\(contractAddress)?a=\(id)"
        guard let url = URL(string: urlString) else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func openIPFS(_ sender: Any) {
        let metadataURL: String = (autograph?.uri)!
        guard let url = URL(string: "\(ipfsGateway)\(metadataURL)") else { return }
        UIApplication.shared.open(url)
    }
    
}
