//
//  AthleteLoginVC.swift
//  autograf
//
//  Created by DevMober on 2.3.21..
//

import UIKit

class SearchAthletesVC: UIViewController {
    
    @IBOutlet weak var tvAthletes: UITableView!
    
    private var celebrities: [Celebrity]? = []
    private var filtered: [Celebrity]? = []
    
    var isFiltered: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Registers the AthleteCell for reuse
        self.tvAthletes.register(UINib(nibName: "AthleteCell", bundle: nil), forCellReuseIdentifier: "AthleteCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIManager.shared.showHUD(view: self.view)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        DigigrafAPI.shared.getCelebrities() {(success, celebrities, msg) in
            UIManager.shared.hideHUD()
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
            if success {
                self.celebrities = celebrities
                self.tvAthletes.reloadData()
            } else {
                UIManager.shared.showAlert(vc: self, title: "Error", message: msg!)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let target = segue.destination as? AutografListVC {
            let celebrity = sender as? Celebrity
            target.cWalletAddress = celebrity?.walletAddress
            target.celebrityId = celebrity?.celebrityId
        }
    }
    
}

extension SearchAthletesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltered {
            return filtered!.count
        }
        return celebrities!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AthleteCell", for: indexPath) as! AthleteCell
        if isFiltered {
            let one = filtered![indexPath.row]
            cell.lblName.text = one.name!
            if let avatar = one.cAvatarUrl, !avatar.isEmpty {
                cell.ivAthlete.sd_setImage(with: URL(string: String(format: "%@%@", DigigrafAPI.avatarImageURL, avatar)), completed: nil)
            }
        } else {
            let one = celebrities![indexPath.row]
            cell.lblName.text = one.name!
            if let avatar = one.cAvatarUrl, !avatar.isEmpty {
                cell.ivAthlete.sd_setImage(with: URL(string: String(format: "%@%@", DigigrafAPI.avatarImageURL, avatar)), completed: nil)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let celebrity: Celebrity?
        if isFiltered {
            celebrity = filtered![indexPath.row]
        } else {
            celebrity = celebrities![indexPath.row]
        }
        
        self.performSegue(withIdentifier: "ToAutografListVC", sender: celebrity)
    }
    
    
}

extension SearchAthletesVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
        self.isFiltered = false
        self.tvAthletes.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filtered = searchText.isEmpty ? celebrities : celebrities?.filter({ (celebrity) -> Bool in
            let name: String = celebrity.name!
            return name.lowercased().contains(searchText.lowercased())
        })
        self.isFiltered = true
        self.tvAthletes.reloadData()
        
    }
    
    
}
