//
//  AutografListVC.swift
//  autograf
//
//  Created by DevMober on 7.3.21..
//

import UIKit
import web3swift
import BigInt
import ObjectMapper
import SDWebImage

class AutografListVC: UIViewController {

    @IBOutlet weak var tvAutographs: UITableView!
    
    private var autographs: [Autograph]? = []
    
    public var cWalletAddress: String?
    public var celebrityId: Int?
    
    let service = Web3swiftService()
    var currentBalance: String?
    var metadata: [Metadata]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tvAutographs.register(UINib(nibName: "AutographCell", bundle: nil), forCellReuseIdentifier: "AutographCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.autographs!.removeAll()

        DispatchQueue.main.async {
            UIManager.shared.showHUD(view: self.view)
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }

        service.getAutographCount { [self] (result) in

            let count = Int(result["0"] as! BigUInt)
            if count > 0 {
                var j = 0
                for i in 1...count {
                    
                    service.getAutographById(id: i) { (result) in
                        j += 1
                        let id = Int(result["id"] as! BigUInt)
                        
                        guard var autograph = Mapper<Autograph>().map(JSON: result ) else {
                            return
                        }
                        autograph.id = id
                        let creator = autograph.creator
                        if let requestId = autograph.requestId, requestId.isEmpty {
                            if let owner = autograph.owner {
                                if self.cWalletAddress! == owner.address && self.cWalletAddress! == creator?.address {
                                    self.autographs?.append(autograph)
                                    // sorts autographs by id
                                    self.autographs = self.autographs!.sorted(by: { (lhs, rhs) -> Bool in
                                        return lhs.id! < rhs.id!
                                    })
                                    
                                }
                            }
                        }
                        
                        if count == j {
                            DispatchQueue.main.async {
                                UIManager.shared.hideHUD()
                                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                                self.tvAutographs.reloadData()
                            }
                        }

                    }

                }

            } 
        }
        
        // gets ETH balance
        service.getETHbalance { (result, _) in
            DispatchQueue.main.async { [self] in
                currentBalance = Web3Utils.formatToEthereumUnits(result!, toUnits: .eth, decimals: 6, decimalSeparator: ".")
            }
        }
        
    }
    
    func onBuy(_ cell: AutographCell) {
        
        if let indexPath = self.tvAutographs.indexPath(for: cell) {
            let autograph = self.autographs![indexPath.row]
            
            let priceDouble = Double(autograph.price!)
            guard let balance = Double(currentBalance!), balance >= priceDouble! else {
                return UIManager.shared.showAlert(vc: self, title: "", message: "Your current balance is not enough to buy. Please add balance.")
            }
            
            UIManager.shared.showHUD(view: self.view)
            
            let alert = UIAlertController(title: "Buy Autograph", message: "Please enter your password to confirm.", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.placeholder = "Password"
                textField.isSecureTextEntry = true
            }
            let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in
                UIManager.shared.hideHUD()
            })
            let confirmAction = UIAlertAction(title: "OK", style: .default) { [weak alert] _ in
                guard let alertController = alert, let password = alertController.textFields?.first else { return }
                let id = autograph.id
                let price = autograph.price
                self.service.buyNFT(tokenId: id!, price: price!, password: password.text!, completion: {(results) in
                    UIManager.shared.hideHUD()
                    if results != nil {
                        UIManager.shared.showTx("Bought Successfully", txhash: results!.hash, with: "To confirm check your transaction hash on Ropsten Etherscan Explorer: \n\(results!.hash)", for: self, dismissOnCompletion: false)
                        self.autographs?.remove(at: indexPath.row)
                        self.tvAutographs.reloadData()
                    }
                    else{
                        UIManager.shared.showAlert(vc: self, title: "Failed", message: "Failed, please try again")
                    }
                })
            }
            alert.addAction(cancel)
            alert.addAction(confirmAction)

            present(alert, animated: true, completion: nil)
            
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let target = segue.destination as? MakeRequestVC {
            target.celebrityId = self.celebrityId
        }
    }
    
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onMakeRequest(_ sender: Any) {
        
        performSegue(withIdentifier: "ToMakeRequest", sender: nil)
        
    }
    
}

extension AutografListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.autographs!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AutographCell", for: indexPath) as! AutographCell
        if let one = autographs?[indexPath.row] {
            let metadataURL = one.uri
            cell.lblPrice.text = one.price! + " ETH"
            DigigrafAPI.shared.getJsonData(metadatahash: metadataURL!) { (metadata) in
                cell.ivAutographs.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.ivAutographs.sd_setImage(with: URL(string: String(format: "%@", metadata!.image!)), completed: nil)
                cell.lblDesc.text = metadata?.name
                self.metadata?.insert(metadata!, at: indexPath.row)
            }
        }        
        cell.getAutograf = self.onBuy
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 280
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let autographVC = UIStoryboard(name: "Customer", bundle: nil).instantiateViewController(identifier: "AutographVC") as? AutographVC
        autographVC?.autograph = autographs![indexPath.row]
        autographVC?.metadata = self.metadata![indexPath.row]
        self.navigationController?.pushViewController(autographVC!, animated: true)
    }
}

