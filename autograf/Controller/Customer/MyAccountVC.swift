//
//  AthleteLoginVC.swift
//  autograf
//
//  Created by DevMober on 2.3.21..
//

import UIKit
import web3swift
import BigInt
import ObjectMapper
import SDWebImage

class MyAccountVC: UIViewController {    
    
    @IBOutlet weak var cvMyCollections: UICollectionView!
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var lblAddressTextView: UITextView!
    @IBOutlet weak var ivPhoto: UIImageView!
    @IBOutlet weak var lblBalance: UILabel!
    
    let service = Web3swiftService()
    private var customer: Customer?
    private var myAddress: String?
    private var metadata: [Metadata]? = []
    private var collectibles: [Autograph]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cvMyCollections.register(UINib(nibName: "MyCollectionCell", bundle: nil), forCellWithReuseIdentifier: "MyCollectionCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        loadBlockchainData()
        
        if let customerId = UserDefaults.standard.value(forKey: "customerId") {
//            DispatchQueue.main.async {
//                UIManager.shared.showHUD(view: self.view)
//            }
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
            DigigrafAPI.shared.getAccount(customerId as! String) { (success, customer, msg) in
//                DispatchQueue.main.async {
//                    UIManager.shared.hideHUD()
//                }
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                if success {
                    self.customer = customer
                    if let name = customer?.customerName, !name.isEmpty {
                        self.tfName.text = name
                    }
                    
                    if let avatar = customer?.avatarUrl, !avatar.isEmpty {
                        print(String(format: "%@%@", DigigrafAPI.avatarImageURL, avatar))
                        self.ivPhoto.sd_setImage(with: URL(string: String(format: "%@%@", DigigrafAPI.avatarImageURL, avatar)), completed: nil)
                    }
                } else {
                    UIManager.shared.showAlert(vc: self, title: "Error", message: msg!)
                }
            }
        }
        
        
    }
    
    func loadBlockchainData() {
        myAddress = Web3swiftService.currentAddressString
        
        self.lblAddressTextView.text = "Address: \n\(myAddress!)"
        
        self.collectibles?.removeAll()
        
        DispatchQueue.main.async {
            UIManager.shared.showHUD(view: self.view)
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
        
        service.getAutographCount { [self] (result) in
            
            let count = Int(result["0"] as! BigUInt)
            if count > 0 {
                var j = 0
                for i in 1...count {
                    
                    service.getAutographById(id: i) { (result) in
                        j += 1
                        let id = Int(result["id"] as! BigUInt)
                        guard var autograph = Mapper<Autograph>().map(JSON: result ) else {
                            return
                        }
                        autograph.id = id
                        let curAddress: String = myAddress!
                        if let owner = autograph.owner  {
                            if curAddress == owner.address {
                                self.collectibles?.append(autograph)
                            }
                        }
                        if count == j {
                            DispatchQueue.main.async {
                                UIManager.shared.hideHUD()
                                // enable user interaction
                                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                                
                                self.cvMyCollections.reloadData()
                            }
                        }
                    }
                    
                }
                
            } else {
                UIManager.shared.hideHUD()
            }
        }
        
        // gets ETH balance
        service.getETHbalance { (result, _) in
            DispatchQueue.main.async { [self] in
                let balance = Web3Utils.formatToEthereumUnits(result!, toUnits: .eth, decimals: 6, decimalSeparator: ".")
                self.lblBalance.text = "Current Balance: " + balance! + " ETH"
            }
        }
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let target = segue.destination as? CustomerAccountSettingVC {
            target.customer = self.customer
        }
    }
    
    @IBAction func onSetting(_ sender: Any) {
        
        self.performSegue(withIdentifier: "ToSetting", sender: customer)
        
    }
    
    
}

extension MyAccountVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectibles!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCollectionCell", for: indexPath) as! MyCollectionCell
        
        let one = collectibles![indexPath.row]
        let metadataURL = one.uri
        cell.lblPrice.text = one.price! + " ETH"
        DigigrafAPI.shared.getJsonData(metadatahash: metadataURL!) { (metadata) in
            cell.ivMyCollectionItem.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.ivMyCollectionItem.sd_setImage(with: URL(string: String(format: "%@", metadata!.image!)), completed: nil)
            self.metadata?.insert(metadata!, at: indexPath.row)
        }
        cell.layer.cornerRadius = 8.0
        cell.clipsToBounds = true
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        return CGSize(width: ((collectionView.frame.size.width)/2 - 10) ,height: ((collectionView.frame.size.width)/2) - 10)
        
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let autographVC = UIStoryboard(name: "Customer", bundle: nil).instantiateViewController(identifier: "AutographVC") as? AutographVC
        autographVC?.autograph = self.collectibles![indexPath.row]
        autographVC?.metadata = self.metadata![indexPath.row]
        self.navigationController?.pushViewController(autographVC!, animated: true)
    }
    
}
