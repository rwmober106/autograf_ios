//
//  AthleteLoginVC.swift
//  autograf
//
//  Created by DevMober on 2.3.21..
//

import UIKit
import web3swift
import SDWebImage
import BigInt
import ObjectMapper

class MarketplaceVC: UIViewController {     
    
    @IBOutlet weak var tvMarketPlace: UITableView!
    
    private var autographs: [Autograph]? = []
    var metadata: [Metadata]? = []
    
    let service = Web3swiftService()
    private var currentBalance: String?
    private var curWalletAddress: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvMarketPlace.register(UINib(nibName: "AutographCell", bundle: nil), forCellReuseIdentifier: "AutographCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadBlockchainData()
        
    }
    
    func loadBlockchainData() {
        self.autographs?.removeAll()
        
        DispatchQueue.main.async {
            UIManager.shared.showHUD(view: self.view)
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
        
        service.getAutographCount { [self] (result) in
            
            let count = Int(result["0"] as! BigUInt)
            if count > 0 {
                var j = 0
                for i in 1...count {
                    
                    service.getAutographById(id: i) { (result) in
                        j += 1
                        let id = Int(result["id"] as! BigUInt)
                        guard var autograph = Mapper<Autograph>().map(JSON: result ) else {
                            return
                        }
                        autograph.id = id
                        let forSale = autograph.forSale! as Bool
                        if forSale {
                            self.autographs?.append(autograph)
                            // sorts autographs by id
                            self.autographs = self.autographs!.sorted(by: { (lhs, rhs) -> Bool in
                                return lhs.id! < rhs.id!
                            })
                            self.tvMarketPlace.reloadData()
                        }
                        if count == j {
                            DispatchQueue.main.async {
                                UIManager.shared.hideHUD()
                                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                            }
                        }
                    }
                    
                }
                
            } else {
                UIManager.shared.hideHUD()
            }
        }
        
        // gets ETH balance
        service.getETHbalance { (result, _) in
            DispatchQueue.main.async { [self] in
                currentBalance = Web3Utils.formatToEthereumUnits(result!, toUnits: .eth, decimals: 6, decimalSeparator: ".")
            }
        }
        
        curWalletAddress = Web3swiftService.currentAddressString
        
    }
    
    func onBuy(_ cell: AutographCell) {
        if let indexPath = self.tvMarketPlace.indexPath(for: cell) {
            let autograph = self.autographs![indexPath.row]
            
            guard self.curWalletAddress! != autograph.owner?.address else {
                return
            }
            
            let priceDouble = Double(autograph.price!)
            guard let balance = Double(currentBalance!), balance >= priceDouble! else {
                return UIManager.shared.showAlert(vc: self, title: "", message: "Your current balance is not enough to buy. Please add balance.")
            }
            
            UIManager.shared.showHUD(view: self.view)
            
            let alert = UIAlertController(title: "Buy Autograph", message: "Please enter your password to confirm.", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.placeholder = "Password"
                textField.isSecureTextEntry = true
            }
            let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in
                UIManager.shared.hideHUD()
            })
            let confirmAction = UIAlertAction(title: "OK", style: .default) { [weak alert] _ in
                guard let alertController = alert, let password = alertController.textFields?.first else { return }
                let id = autograph.id
                let price = autograph.price
                self.service.buyNFT(tokenId: id!, price: price!, password: password.text!, completion: {(results) in
                    UIManager.shared.hideHUD()
                    if results != nil {
                        UIManager.shared.showTx("Bought Successfully", txhash: results!.hash, with: "To confirm check your transaction hash on Ropsten Etherscan Explorer: \n\(results!.hash)", for: self, dismissOnCompletion: false)
                        self.autographs?.remove(at: indexPath.row)
                        self.tvMarketPlace.reloadData()
                    }
                    else{
                        UIManager.shared.showAlert(vc: self, title: "Failed", message: "Failed, please try again")
                    }
                })
            }
            alert.addAction(cancel)
            alert.addAction(confirmAction)

            present(alert, animated: true, completion: nil)
            
            
        }
    }
    
}

extension MarketplaceVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.autographs!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AutographCell", for: indexPath) as! AutographCell
        if let one = autographs?[indexPath.row] {
            let metadataURL = one.uri
            cell.lblPrice.text = one.price! + " ETH"
            DigigrafAPI.shared.getJsonData(metadatahash: metadataURL!) { (metadata) in
                cell.ivAutographs.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.ivAutographs.sd_setImage(with: URL(string: String(format: "%@", metadata!.image!)), completed: nil)
                cell.lblDesc.text = metadata?.name
                self.metadata?.insert(metadata!, at: indexPath.row)
            }
            
            if self.curWalletAddress! == one.owner?.address {
                let buyButton: UIButton = cell.viewWithTag(100) as! UIButton
                buyButton.isEnabled = false
                buyButton.alpha = 0.5
            }
            
        }
        cell.getAutograf = self.onBuy
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 280
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let autographVC = UIStoryboard(name: "Customer", bundle: nil).instantiateViewController(identifier: "AutographVC") as? AutographVC
        autographVC?.autograph = autographs![indexPath.row]
        autographVC?.metadata = self.metadata![indexPath.row]
        self.navigationController?.pushViewController(autographVC!, animated: true)
    }
}
