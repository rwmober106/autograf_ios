//
//  MyRequestsVC.swift
//  autograf
//
//  Created by DevMober on 8.4.21..
//

import UIKit

class MyRequestsVC: UIViewController {

    @IBOutlet weak var tvRequests: UITableView!
    
    private var requests: [Request]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Registers the AthleteCell for reuse
        self.tvRequests.register(UINib(nibName: "AthleteCell", bundle: nil), forCellReuseIdentifier: "AthleteCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let customerId = UserDefaults.standard.value(forKey: "customerId") {
            UIManager.shared.showHUD(view: self.view)
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
            DigigrafAPI.shared.getMyRequests(customerId as! String) { (success, requests, msg) in
                UIManager.shared.hideHUD()
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                if success {
                    self.requests = requests!
                    self.tvRequests.reloadData()
                } else {
                    UIManager.shared.showAlert(vc: self, title: "Error", message: msg!)
                }
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let target = segue.destination as? AutografListVC {
//            let celebrity = sender as? Celebrity
//            target.cWalletAddress = celebrity?.walletAddress
//            target.celebrityId = celebrity?.celebrityId
//        }
    }
    
}

extension MyRequestsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requests!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AthleteCell", for: indexPath) as! AthleteCell
        let one = requests![indexPath.row]
        cell.lblName.text = one.celebrityName
        if one.status == 0 {
            cell.lblStatus.text = "pending..."
        } else if one.status == 1 {
            cell.lblStatus.text = "accepted"
        } else {
            cell.lblStatus.text = "declined"
        }
        
        if let avatar = one.cAvatarUrl, !avatar.isEmpty {
            cell.ivAthlete.sd_setImage(with: URL(string: String(format: "%@%@", DigigrafAPI.avatarImageURL, avatar)), completed: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let request = requests![indexPath.row]
        if request.status == 0 {
            let makeRequestVC = UIStoryboard(name: "Customer", bundle: nil).instantiateViewController(identifier: "MakeRequestVC") as? MakeRequestVC
            makeRequestVC?.imageUrl = request.imageUrl
            makeRequestVC?.message = request.message
            makeRequestVC!.myRequestsViewController = self
            self.present(makeRequestVC!, animated: true, completion: nil)
            
        } else if request.status == 1 {
            let autographVC = UIStoryboard(name: "Customer", bundle: nil).instantiateViewController(identifier: "AutographVC") as? AutographVC
            if let requestId = request.requestId {
                autographVC?.requestId = requestId
            }
            self.navigationController?.pushViewController(autographVC!, animated: true)
            
        }
    }
    
    
}

