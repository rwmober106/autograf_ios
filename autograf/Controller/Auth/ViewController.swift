//
//  ViewController.swift
//  autograf
//
//  Created by DevMober on 24.3.21..
//

import UIKit

class ViewController: UIViewController {

    var newWalletType: CreationType = .createWallet
    public var celebrity: Celebrity?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CreateWallet" {
            newWalletType = .createWallet
        } else {
            newWalletType = .importWallet
        }
        
        if let createWalletController = segue.destination as? WalletGenerationVC {
            createWalletController.creationType = newWalletType
            createWalletController.celebrity = celebrity
        }
    }
    

}
