//
//  ViewController.swift
//  autograf
//
//  Created by DevMober on 2.3.21..
//

import UIKit

class WalletGenerationVC: UIViewController {

    @IBOutlet weak var passwordsDontMatch: UILabel!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    @IBOutlet weak var enterPrivateKeyTextField: UITextField!
    @IBOutlet weak var privateKeyView: UIView!
    
    var creationType: CreationType  = .createWallet
    let keysService: KeysService = KeysService()
    let localStorage = LocalDatabase()
    let web3service: Web3swiftService = Web3swiftService()
    let walletGenerationController = WalletGenerationController()
    public var celebrity: Celebrity?
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        createButton.setTitle(creationType.title(), for: .normal)
        createButton.isEnabled = false
        createButton.alpha = 0.5
        passwordsDontMatch.isHidden = true
        privateKeyView.isHidden = false
        
        
        //test
//        enterPrivateKeyTextField.text = "bdd173cc492a994d8786463c07b1d1e82f13f036acf66ad62f1aa7c1ff9e8c37" // contract owner address
        
        let viewcontrollers = self.navigationController?.children
        if viewcontrollers![1] is LoginVC {
            // for celebrity
            enterPrivateKeyTextField.text = "b8c127d8a2e6cc7af79f00c8509889edf7a111c9370ce041ca8eb42c8141451c"
        } else {
            // for customer
            enterPrivateKeyTextField.text = "0e11b3dbb44f40868b024740256bb7df658967fdc0c244dc96bc12e2e89726e2"
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if creationType == .createWallet {
            enterPrivateKeyTextField.isUserInteractionEnabled = false
            privateKeyView.isHidden = true
        } 
    }
    
    func createWallet() {
        guard passwordTextField.text == repeatPasswordTextField.text else {
            passwordsDontMatch.isHidden = false
            return
        }
        passwordsDontMatch.isHidden = true 
        
        walletGenerationController.createWallet(with: creationType, password: passwordTextField.text, key: enterPrivateKeyTextField.text) { (error, wallet) in
            guard error == nil else {
                UIManager.shared.showError(error, for: self)
                return
            }
            let walletAddress = wallet?.address
            let viewcontrollers = self.navigationController?.children
            if viewcontrollers![1] is LoginVC {
                
                if (self.celebrity != nil) {
                    let celebrityId = String((self.celebrity?.celebrityId)!)
                    
                    if let firstName = self.celebrity?.firstName!,
                       let lastName = self.celebrity?.lastName!,
                       let email = self.celebrity?.email!,
                       let phoneNumber = self.celebrity?.phoneNumber!,
                       let celebrityType = self.celebrity?.celebrityType!,
                       let cAvatarUrl = self.celebrity?.cAvatarUrl! {
                        UIManager.shared.showHUD(view: self.view)
                        DigigrafAPI.shared.celebrityUpdate(firstName: firstName, lastName: lastName, email: email, phoneNumber: phoneNumber, celebrityType: celebrityType, cAvatarUrl: cAvatarUrl, walletAddress: walletAddress!, celebrityId: celebrityId) { (error, celebrity, msg) in
                            UIManager.shared.hideHUD()
                            if !error {
                                UserDefaults.standard.set(celebrityId, forKey: "celebrityId")
                                UserDefaults.standard.set(false, forKey: "isCustomer")
                                self.performSegue(withIdentifier: "ToAthlete", sender: nil)
                            } else {
                                UIManager.shared.showAlert(vc: self, title: "Error", message: msg!)
                            }
                        }
                    }
                }
                
            } else {
                UIManager.shared.showHUD(view: self.view)
                DigigrafAPI.shared.customerRegister(walletAddress!, "", "") { (success, customer, msg) in
                    UIManager.shared.hideHUD()
                    if success {
                        let customerId = String((customer?.customerId)!)
                        UserDefaults.standard.set(customerId, forKey: "customerId")
                        UserDefaults.standard.set(true, forKey: "isCustomer")
                        self.performSegue(withIdentifier: "ToCustomer", sender: nil)
                    } else {
                        UIManager.shared.showAlert(vc: self, title: "Error", message: msg!)
                    }
                }
                
            }
        }
        
    }
    
    @IBAction func createWalletButtonTapped(_ sender: UIButton) {
        createWallet()
        UIView.animate(withDuration: 0.05) {
            sender.transform = CGAffineTransform.identity
        }
    }
    

    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension WalletGenerationVC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        textField.returnKeyType = createButton.isEnabled ? UIReturnKeyType.done : .next
//        textField.textColor = UIColor(red:0.79, green:0.62, blue:0.40, alpha:1.0)
        if textField == passwordTextField || textField == repeatPasswordTextField {
            passwordsDontMatch.isHidden = true
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = (textField.text ?? "") as NSString
        let futureString = currentText.replacingCharacters(in: range, with: string) as String
        createButton.isEnabled = false
        
        switch textField {
        case enterPrivateKeyTextField:
            if passwordTextField.text == repeatPasswordTextField.text &&
                !(passwordTextField.text?.isEmpty ?? true) &&
                !futureString.isEmpty && ((passwordTextField.text?.count)! > 4) {
                createButton.isEnabled = true
            }
        case passwordTextField:
            if !futureString.isEmpty &&
                futureString == repeatPasswordTextField.text ||
                repeatPasswordTextField.text?.isEmpty == true {
                passwordsDontMatch.isHidden = true
                createButton.isEnabled = (!(enterPrivateKeyTextField.text?.isEmpty ?? true) || creationType == .createWallet)
            } else {
                passwordsDontMatch.isHidden = false
                createButton.isEnabled = false
            }
        case repeatPasswordTextField:
            if !futureString.isEmpty &&
                futureString == passwordTextField.text {
                passwordsDontMatch.isHidden = true
                createButton.isEnabled = (!(enterPrivateKeyTextField.text?.isEmpty ?? true) || creationType == .createWallet)
            } else {
                passwordsDontMatch.isHidden = false
                createButton.isEnabled = false
            }
        default:
            createButton.isEnabled = false
            passwordsDontMatch.isHidden = false
        }
        
        createButton.alpha = createButton.isEnabled ? 1.0 : 0.5
        textField.returnKeyType = createButton.isEnabled ? UIReturnKeyType.done : .next
        
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.textColor = UIColor.darkGray
        
        guard textField == repeatPasswordTextField ||
            textField == passwordTextField else {
                return true
        }
        if (!(passwordTextField.text?.isEmpty ?? true) ||
            !(repeatPasswordTextField.text?.isEmpty ?? true)) &&
            passwordTextField.text != repeatPasswordTextField.text {
            passwordsDontMatch.isHidden = false
            passwordsDontMatch.text = "Passwords don't match"
//            repeatPasswordTextField.textColor = UIColor.red
//            passwordTextField.textColor = UIColor.red
        } else if (!(passwordTextField.text?.isEmpty ?? true) ||
            !(repeatPasswordTextField.text?.isEmpty ?? true)) &&
            ((passwordTextField.text?.count)! < 5) {
            passwordsDontMatch.isHidden = false
            passwordsDontMatch.text = "Password is too short"
//            repeatPasswordTextField.textColor = UIColor.red
//            passwordTextField.textColor = UIColor.red
        } else {
//            repeatPasswordTextField.textColor = UIColor.darkGray
//            passwordTextField.textColor = UIColor.darkGray
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done && createButton.isEnabled && ((passwordTextField.text?.count)! > 4) {
            createWallet()
        } else if textField.returnKeyType == .next {
//            let index = textFields.index(of: textField) ?? 0
//            let nextIndex = (index == textFields.count - 1) ? 0 : index + 1
//            textFields[nextIndex].becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
}

