//
//  AthleteLoginVC.swift
//  autograf
//
//  Created by DevMober on 2.3.21..
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfUsername.text = "test1@gmail.com"
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let target = segue.destination as? ViewController {
            
            let one = sender as? Celebrity
            target.celebrity = one
        }
        
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onLogin(_ sender: Any) {
        
        guard let email = tfUsername.text, !email.isEmpty else {
            UIManager.shared.showAlert(vc: self, title: "", message: "Please input email address")
            return
        }
        guard let password = tfPassword.text, !password.isEmpty else {
            UIManager.shared.showAlert(vc: self, title: "", message: "Please input password")
            return
        }
        UIManager.shared.showHUD(view: self.view)
        DigigrafAPI.shared.celebrityLogin(email, password) { (success, celebrity, msg) in
            UIManager.shared.hideHUD()
            
            self.performSegue(withIdentifier: "ToViewController", sender: celebrity)
            
        }
        
        
        

        
    }
    
    
    
}
