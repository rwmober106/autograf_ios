//
//  WalletGenerationController.swift
//  WannaBet
//
//  Created by Matt Johnson on 11/27/18.
//  Copyright © 2018 Matt Johnson. All rights reserved.
//

import Foundation
import web3swift

class WalletGenerationController {
    
    let localStorage = LocalDatabase()
    let keysService: KeysService = KeysService()
    
    func createWallet(with mode: CreationType,
                      password: String?,
                      key: String?,
                      completion: @escaping (Error?, KeyWalletModel?) -> Void) {
        guard let password = password else {
            completion(Errors.noPassword, nil)
            return
        }
        switch mode {
        case .createWallet:
            keysService.createNewWallet(password: password) { (wallet, error) in
                if let error = error {
                    completion(error, nil)
                } else {
                    self.localStorage.saveWallet(isRegistered: false, wallet: wallet!) { (error) in
                        completion(error, wallet)
                    }
                }
            }
        case .importWallet:
            guard let key = key else {
                completion(Errors.noKey, nil)
                return
            }
            keysService.addNewWalletWithPrivateKey(key: key, password: password) { (wallet, error) in
                if let error = error {
                    completion(error, nil)
                } else {
                    let walletStrAddress = wallet?.address
                    let walletAddress = EthereumAddress(walletStrAddress!)

                    self.localStorage.saveWallet(isRegistered: true, wallet: wallet!) { (error) in
                                completion(error, wallet)
                            }
                }
            }
        }
    }
}
