//
//  RequestsVC.swift
//  autograf
//
//  Created by DevMober on 7.3.21..
//

import UIKit
import SDWebImage

class RequestsVC: UIViewController {

    @IBOutlet weak var tvRequest: UITableView!
    
    private var requests = [Request]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tvRequest.register(UINib(nibName: "RequestCell", bundle: nil), forCellReuseIdentifier: "RequestCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let celebrityId = UserDefaults.standard.value(forKey: "celebrityId") {
            
            UIManager.shared.showHUD(view: self.view)
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
            
            DigigrafAPI.shared.getRequests(celebrityId as! String) { (success, requests, msg) in
                UIManager.shared.hideHUD()
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                if success {
                    self.requests = requests!
                    // sort requests by request id
                    self.requests = self.requests.sorted(by: { (lhs, rhs) -> Bool in
                        return lhs.requestId! < rhs.requestId!
                    })
                    self.tvRequest.reloadData()
                } else {
                    UIManager.shared.showAlert(vc: self, title: "Error", message: msg!)
                }
            }
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let target = segue.destination as? RequestDetailsVC {
            let row = (sender as! NSIndexPath).row
            let request = requests[row]
            target.request = request
        }
        
    }
    

}

extension RequestsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return requests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
        let request = requests[indexPath.row]
        if let name = request.customerName, !name.isEmpty {
            cell.lblName.text = name
        }
//        cell.lblWAddress.text = request.wallet_address
        if let avatar = request.avatarUrl, !avatar.isEmpty {
            cell.ivAvatar.sd_setImage(with: URL(string: String(format: "%@%@", DigigrafAPI.avatarImageURL, avatar)), completed: nil)
        }
        cell.lblDesc.text = request.message
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "ToRequestDetails", sender: indexPath)
    }
}
