//
//  PreviewVC.swift
//  autograf
//
//  Created by DevMober on 12.3.21..
//

import UIKit
import SDWebImage
import web3swift

class RequestDetailsVC: UIViewController {

    @IBOutlet weak var ivPreview: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblNoAddOn: UILabel!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnDecline: UIButton!
    
    public var request: Request?
    let service = Web3swiftService()
    var currentBalance: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // inits UI
        lblNoAddOn.layer.borderWidth = 1
        lblNoAddOn.layer.borderColor = UIColor.white.cgColor
        lblNoAddOn.layer.cornerRadius = 8
        lblNoAddOn.clipsToBounds = true
        
        // sets value
        lblMessage.text = request?.message
        if let image = request?.imageUrl, !image.isEmpty {
            self.ivPreview.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.ivPreview.sd_setImage(with: URL(string: String(format: "%@%@", DigigrafAPI.nftImageURL, image)), completed: nil)
        }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // gets ETH balance
        UIManager.shared.showHUD(view: self.view)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        service.getETHbalance { (result, _) in
            DispatchQueue.main.async { [self] in
                UIManager.shared.hideHUD()
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                currentBalance = Web3Utils.formatToEthereumUnits(result!, toUnits: .eth, decimals: 6, decimalSeparator: ".")
            }
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onAccept(_ sender: Any) {
        
        guard let balance = Double(currentBalance!), balance > 0.01 else {
            return UIManager.shared.showAlert(vc: self, title: "", message: "Your current balance is not enough to mint. Please add balance.")
        }
        
        UIManager.shared.showHUD(view: self.view)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        let id: String = String((request?.requestId)!)
        DigigrafAPI.shared.handleRequest(requestId: id, status: 1) {(success, msg) in
            UIManager.shared.hideHUD()
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
            if success {
                self.view.makeToast(message: "Accepted Successfully")
                self.performSegue(withIdentifier: "FromRequestDetails", sender: nil)
                
                // disables buttons
                self.btnAccept.isEnabled = false
                self.btnDecline.isEnabled = false
                self.btnAccept.alpha = 0.5
                self.btnDecline.alpha = 0.5
                
            } else {
                UIManager.shared.showAlert(vc: self, title: "Error", message: msg!)
            }
        }
        
    }
    
    @IBAction func onDecline(_ sender: Any) {
        UIManager.shared.showHUD(view: self.view)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        let id: String = String((request?.requestId)!)
        DigigrafAPI.shared.handleRequest(requestId: id, status: 2) {(success, msg) in
            UIManager.shared.hideHUD()
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
            if success {
                self.view.makeToast(message: "Declined Successfully")
                self.navigationController?.popViewController(animated: true)
            } else {
                UIManager.shared.showAlert(vc: self, title: "Error", message: msg!)
            }
        }
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let target = segue.destination as? SignatureVC {
            
            if self.ivPreview.image != nil {
                target.hidesBottomBarWhenPushed = true
                target.addonImage = self.ivPreview.image!
                target.desc = request?.message
                let id = String((request?.requestId)!)
                target.requestId = id
            }
            
        }
    }
    

}
