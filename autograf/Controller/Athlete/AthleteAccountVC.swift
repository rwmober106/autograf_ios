//
//  AthleteAccountVC.swift
//  autograf
//
//  Created by DevMober on 7.3.21..
//

import UIKit
import web3swift

class AthleteAccountVC: UIViewController {

    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var addressLabel: UITextView!
    
    let service = Web3swiftService()
    let keysService = KeysService()
    let localDatabase = LocalDatabase()
    var myAddress: String?

    override func viewDidLoad() {
        super.viewDidLoad()
          
        loadBlockchainData()
        
        
    }
    
    func loadBlockchainData() {
        // gets wallet address
        myAddress = Web3swiftService.currentAddressString
        self.addressLabel.text = "Address: \n\(myAddress!)"
        
        // gets ETH balance
        UIManager.shared.showHUD(view: self.view)
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        service.getETHbalance { (result, _) in
            DispatchQueue.main.async {
                UIManager.shared.hideHUD()
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                let ethUnits = Web3Utils.formatToEthereumUnits(result!, toUnits: .eth, decimals: 6, decimalSeparator: ".")
                self.balanceLabel.text = "Current ETH Balance: \n" + ethUnits!
                
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func privateKeyAlert(privateKey: String) {
        let alert = UIAlertController(title: "Private key", message: privateKey, preferredStyle: UIAlertController.Style.alert)
        
        let copyAction = UIAlertAction(title: "Copy", style: .default) { (_) in
            let pasteboard = UIPasteboard.general
            pasteboard.string = privateKey
        }
        
        let closeAction = UIAlertAction(title: "Close", style: .cancel) { (_) in
        }
        alert.addAction(copyAction)
        alert.addAction(closeAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func showPrivateKey(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Show private key", message: nil, preferredStyle: UIAlertController.Style.alert)
        
        alert.addTextField { (textField) in
            textField.isSecureTextEntry = true
            textField.placeholder = "Enter your password"
        }
        
        let enterPasswordAction = UIAlertAction(title: "Enter", style: .default) { (_) in
            let passwordTextField = alert.textFields![0] as UITextField
            if let privateKey = self.keysService.getWalletPrivateKey(password: passwordTextField.text!) {
                self.privateKeyAlert(privateKey: privateKey)
                
            } else {
                UIManager.shared.showAlert(vc: self, title: "", message: "Wrong Password")
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            
        }
        
        alert.addAction(enterPasswordAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
   
    @IBAction func logout(_ sender: UIButton) {
        let alert = UIAlertController(title: "Log out", message: "Are you sure you would like to log out? You will have to create a new wallet or import an existing one again.", preferredStyle: .alert)
        
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        let confirmAction = UIAlertAction(title: "Logout", style: .destructive) { _ in
            
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
        
            self.localDatabase.deleteWallet { (error) in
                if error == nil {
                    
                    guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                          let sceneDelegate = windowScene.delegate as? SceneDelegate
                    else {
                        return
                    }
                    sceneDelegate.goToRoot()
                } else {
                    UIManager.shared.showAlert(vc: self, title: "Error", message: error!.localizedDescription)
                }
            }
        }
        alert.addAction(cancel)
        alert.addAction(confirmAction)
        present(alert, animated: true)
    }
    
    @IBAction func addBalance(_ sender: Any) {
        let alert = UIAlertController(title: "Add Balance to your Wallet?", message: "Clicking Ok will open a link to the Oasis Devnet Faucet in Safari and copy your wallet address to your pasteboard for signup.", preferredStyle: .alert)

        let enterPasswordAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            let pasteboard = UIPasteboard.general
            pasteboard.string = self.myAddress!
            guard let url = URL(string: "https://faucet.oasiscloud.io/") else { return }
            UIApplication.shared.open(url)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            
        }
        
        alert.addAction(enterPasswordAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
        
       
    }

}



