//
//  MainVC.swift
//  autograf
//
//  Created by DevMober on 5.3.21..
//

import UIKit
import web3swift
import ObjectMapper
import BigInt
import SDWebImage

class MainVC: UIViewController {

    @IBOutlet weak var cvAutographs: UICollectionView!
    @IBOutlet weak var lblCountOfAutogrpahsByMe: UILabel!
    
    let service = Web3swiftService()
    private var currentBalance: String?
    private var myAddress: String?
    private var totalCount: String?
    private var collectibles: [Autograph]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cvAutographs.register(UINib(nibName: "AutographCVCell", bundle: nil), forCellWithReuseIdentifier: "AutographCVCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        loadBlockchainData()
        
    }
    
    func loadBlockchainData() {
        
        // gets wallet address
        myAddress = Web3swiftService.currentAddressString
        
        // gets ETH balance
        service.getETHbalance { (result, _) in
            DispatchQueue.main.async { [self] in
                currentBalance = Web3Utils.formatToEthereumUnits(result!, toUnits: .eth, decimals: 6, decimalSeparator: ".")
            }
        }
        
        // load my collectibles
        self.collectibles?.removeAll()
        
        DispatchQueue.main.async {
            UIManager.shared.showHUD(view: self.view)
            // disable user interaction during background task
            self.tabBarController?.tabBar.isUserInteractionEnabled = false
        }
        
        service.getAutographCount { [self] (result) in
            
            let count = Int(result["0"] as! BigUInt)
            if count > 0 {
                self.totalCount = String(count)
                var c = 0
                var j = 0
                for i in 1...count {
                    
                    service.getAutographById(id: i) { (result) in
                        j += 1
                        let id = Int(result["id"] as! BigUInt)
                        guard var autograph = Mapper<Autograph>().map(JSON: result ) else {
                            return
                        }
                        autograph.id = id
                        let curAddress: String = myAddress!
                        if let owner = autograph.owner {
                            if curAddress == owner.address {
                                self.collectibles?.append(autograph)
                                // sorts collectibles by id
                                self.collectibles = self.collectibles!.sorted(by: { (lhs, rhs) -> Bool in
                                    return lhs.id! < rhs.id!
                                })
                                
                            }
                        }
                        if let creator = autograph.creator {
                            
                            if curAddress == creator.address {
                                c += 1
                            }
                            self.lblCountOfAutogrpahsByMe.text = String(c)
                        }
                        
                        if count == j {
                            DispatchQueue.main.async {
                                UIManager.shared.hideHUD()
                                // enable user interaction
                                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                                
                                self.cvAutographs.reloadData()
                            }
                        }
                        
                    }
                    
                }
                
            } else {
                self.lblCountOfAutogrpahsByMe.text = "0"
                UIManager.shared.hideHUD()
            }
        }
        
    }
    
//    func getMyCellectibles() {
//
//        self.collectibles?.removeAll()
//
//        UIManager.shared.showHUD(view: self.view)
//        service.getAutographCount { [self] (result) in
//            UIManager.shared.hideHUD()
//            let count = Int(result["0"] as! BigUInt)
//            if count > 0 {
//                self.totalCount = String(count)
//                var c = 0
//                for i in 1...count {
//                    UIManager.shared.showHUD(view: self.view)
//                    service.getAutographById(id: i) { (result) in
//                        UIManager.shared.hideHUD()
//
//                        let id = Int(result["id"] as! BigUInt)
//                        guard var autograph = Mapper<Autograph>().map(JSON: result ) else {
//                            return
//                        }
//                        autograph.id = id
//                        let curAddress: String = myAddress!
//                        if let owner = autograph.owner {
//                            if curAddress == owner.address {
//                                self.collectibles?.append(autograph)
//                                self.cvAutographs.reloadData()
//                            }
//                        }
//                        if let creator = autograph.creator {
//
//                            if curAddress == creator.address {
//                                c += 1
//                            }
//                            self.lblCountOfAutogrpahsByMe.text = String(c)
//                        }
//
//                    }
//
//                }
//
//            } else {
//                self.lblCountOfAutogrpahsByMe.text = "0"
//                UIManager.shared.hideHUD()
//            }
//        }
//
//    }
    
    
    
    @IBAction func onMakeAutograph(_ sender: Any) {
        
        guard let balance = Double(currentBalance!), balance > 0.01 else {
            return UIManager.shared.showAlert(vc: self, title: "", message: "Your current balance is not enough to mint. Please add balance.")
        }
        performSegue(withIdentifier: "ToSignatureVC", sender: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MainVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectibles!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AutographCVCell", for: indexPath) as! AutographCVCell
        
        let autograph = self.collectibles![indexPath.row]
        let metadataURL = autograph.uri
        DigigrafAPI.shared.getJsonData(metadatahash: metadataURL!) { (metadata) in
            cell.ivAutograph.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.ivAutograph.sd_setImage(with: URL(string: String(format: "%@", metadata!.image!)), completed: nil)
        }
        cell.lblPrice.text = autograph.price! + " ETH"
        cell.layer.cornerRadius = 8.0
        cell.clipsToBounds = true
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        return CGSize(width: ((collectionView.frame.size.width)/2 - 10) ,height: ((collectionView.frame.size.width)/2) - 10)
        
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        
    }
    
}

