//
//  SignatureVC.swift
//  autograf
//
//  Created by DevMober on 3.3.21..
//

import UIKit
import PencilKit
import web3swift

class SignatureVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var canvasView: PKCanvasView!
    @IBOutlet weak var underlayView: UIImageView!
    @IBOutlet weak var navigationBar: UIView!
    @IBOutlet weak var previewContainer: UIView!
    @IBOutlet weak var previewView: UIImageView!
       
    public lazy var addonImage = UIImage()
    public var desc: String?
    public var requestId: String?
    
    let service = Web3swiftService()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        previewContainer.isHidden = true
        setupCanvasView()
        
        if let viewcontrollers = self.navigationController?.children {
            if viewcontrollers[1] is RequestDetailsVC {
                
                let newSize = self.addonImage.size
                self.underlayView.contentMode = .scaleToFill
                self.underlayView.frame = CGRect(origin: CGPoint.zero, size: newSize)
                self.underlayView.image = self.addonImage
                self.underlayView.layer.borderColor = UIColor.orange.cgColor
                self.underlayView.layer.borderWidth = 1.0
                
                let contentSize = newSize
                self.canvasView.contentSize = contentSize
                self.underlayView.frame = CGRect(origin: CGPoint.zero, size: contentSize)
                let margin = (self.canvasView.bounds.size - contentSize) * 0.5
                let insets = [margin.width, margin.height].map { $0 > 0 ? $0 : 0 }
                self.canvasView.contentInset = UIEdgeInsets(top: insets[1], left: insets[0], bottom: insets[1], right: insets[0])
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupCanvasView()
        self.canvasView.sendSubviewToBack(self.underlayView)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.canvasView.becomeFirstResponder()
        self.canvasView.tool = PKInkingTool(.pen)
    }
    
    private func setupCanvasView() {
        
        self.canvasView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            canvasView.topAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: 16),
            canvasView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            canvasView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            canvasView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
        
        self.canvasView.contentInsetAdjustmentBehavior = .never
        self.canvasView.layer.borderColor = UIColor.red.cgColor
        self.canvasView.layer.borderWidth = 2.0
        self.canvasView.delegate = self
        self.canvasView.maximumZoomScale = 2.0
        self.canvasView.isOpaque = false
        self.canvasView.backgroundColor = .clear
        self.canvasView.contentOffset = CGPoint.zero

        if #available(iOS 14.0, *) {
            self.canvasView.drawingPolicy = .anyInput
        } else {
            self.canvasView.allowsFingerDrawing = true
        }
        
        // PKToolPicker
        if let window = UIApplication.shared.windows.first, let toolPicker = PKToolPicker.shared(for: window) {
            // set tool picker visible
            toolPicker.setVisible(true, forFirstResponder: self.canvasView)
            toolPicker.addObserver(self.canvasView)
            toolPicker.addObserver(self)
            self.canvasView.becomeFirstResponder()
        }
    }
    
    private func mintNFT(tokenURI: String, price: String, requestId: String) {
//        UIManager.shared.showHUD(view: self.view)
        let alert = UIAlertController(title: "Mint Your Autograph NFT", message: "Please enter your password to confirm.", preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = "Password"
            textField.isSecureTextEntry = true
        }
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { _ in
//            UIManager.shared.hideHUD()
        })
        let confirmAction = UIAlertAction(title: "OK", style: .default) { [weak alert] _ in
            guard let alertController = alert, let password = alertController.textFields?.first else { return }
            self.service.mintNFT(tokenURI: tokenURI, price: price, requestId: requestId, password: password.text!, completion: {(results) in
//                UIManager.shared.hideHUD()
                if results != nil {
                    UIManager.shared.showTx("Minted Your Autograph", txhash: results!.hash, with: "To confirm check your transaction hash on Ropsten Etherscan Explorer: \n\(results!.hash)", for: self, dismissOnCompletion: false)
                }
                else{
                    UIManager.shared.showAlert(vc: self, title: "Mint Failed", message: "Failed, please try again")
                }
            })
        }
        alert.addAction(cancel)
        alert.addAction(confirmAction)

        present(alert, animated: true, completion: nil)

    }
    
    @IBAction func onClear(_ sender: Any) {
        self.canvasView.drawing = PKDrawing()
    }
    
    @IBAction func onPreview (_ sender: Any) {
        
        // sets PKToolPicker non visible
        if let window = UIApplication.shared.windows.first, let toolPicker = PKToolPicker.shared(for: window) {
            // set tool picker visible
            toolPicker.setVisible(false, forFirstResponder: self.canvasView)
            toolPicker.addObserver(self.canvasView)
            toolPicker.addObserver(self)
            self.canvasView.becomeFirstResponder()
        }
        
        previewContainer.isHidden = false
        if self.underlayView.image != nil {
            let size = self.addonImage.size
            let bottomImage = self.underlayView.image
            let bounds = CGRect(origin: CGPoint.zero, size: self.canvasView.contentSize)
            let topImage = self.canvasView.drawing.image(from: bounds, scale: UIScreen.main.scale)
            UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
            bottomImage!.draw(in: CGRect(origin: CGPoint.zero, size: size))
            topImage.draw(in: CGRect(origin: CGPoint.zero, size: size))
            let newImage = UIGraphicsGetImageFromCurrentImageContext()
            previewView.image = newImage
            UIGraphicsEndImageContext()
            
        } else {
            let previewImage = self.canvasView.drawing.image(from: self.canvasView.bounds, scale: UIScreen.main.scale)
            previewView.image = previewImage
            
        }
        
    }
    
    @IBAction func onPreviewCancel(_ sender: Any) {
        previewContainer.isHidden = true
    }
    
    @IBAction func onSetPrice(_ sender: Any) {
        
        let alertController = UIAlertController(title: nil, message: "Please add some information and set the price", preferredStyle: .alert)
        alertController.addTextField { (tfName) in
            tfName.placeholder = "Autograph Title"
        }
        alertController.addTextField { (tfDesc) in
            tfDesc.placeholder = "Autograph Description"
        }
        alertController.addTextField { (tfPrice) in
            tfPrice.keyboardType = .decimalPad
            tfPrice.placeholder = "0.2 ETH"
        }
        let releaseAction = UIAlertAction(title: "Release", style: .default) { (action) in
            let title = alertController.textFields![0].text
            let desc = alertController.textFields![1].text
            
            // checks if price is a valid value
            guard let price = alertController.textFields![2].text, price.doubleValue != nil && (price.doubleValue)! > 0.001 else {
                return UIManager.shared.showAlert(vc: self, title: "", message: "Please add right price")
            }
            let p = Double(price)
            let price1 = String(p!)
            
            self.previewContainer.isHidden = true
            let imageData = self.previewView.image!.jpegData(compressionQuality: 1.0)
            let base64encodeImage: String = (imageData?.base64EncodedString(options: .lineLength64Characters))!
            let celebrityId: String = UserDefaults.standard.value(forKey: "celebrityId") as! String;
            
            UIManager.shared.showHUD(view: self.view)
            DigigrafAPI.shared.uploadAutograph(celebrityId, title!, desc!, base64encodeImage, price1) { (success, metadataURI, msg) in
                UIManager.shared.hideHUD()
                if success {
                    DispatchQueue.main.async {
                        self.view.makeToast(message: "Successfully Uploaded Your Authgraph to IPFS!")
                        if self.requestId == nil {
                            self.mintNFT(tokenURI: metadataURI!, price: price, requestId: "")
                        } else {
                            self.mintNFT(tokenURI: metadataURI!, price: price, requestId: self.requestId!)
                        }
                        
                    }
                } else {
                    UIManager.shared.showAlert(vc: self, title: "Error", message: msg!)
                }

            }

            
        }
        alertController.addAction(releaseAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func onAdd(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {

        self.addonImage = UIImage()
        self.underlayView.image = nil
        self.canvasView.drawing = PKDrawing()
        
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {

            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    //MARK:- UIImagePickerViewDelegate.
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        self.dismiss(animated: true) { [weak self] in

            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            
            // Resizes image
            let newSize = image.size * 0.5
            self!.addonImage = image.resizeUI(size: newSize)!
            
            self?.underlayView.contentMode = .scaleToFill
            self?.underlayView.frame = CGRect(origin: CGPoint.zero, size: newSize)
            self?.underlayView.image = self?.addonImage
            self?.underlayView.layer.borderColor = UIColor.orange.cgColor
            self?.underlayView.layer.borderWidth = 1.0
            
            let contentSize = newSize
            self?.canvasView.contentSize = contentSize
            self?.underlayView.frame = CGRect(origin: CGPoint.zero, size: contentSize)
            let margin = (self!.canvasView.bounds.size - contentSize) * 0.5
            let insets = [margin.width, margin.height].map { $0 > 0 ? $0 : 0 }
            self?.canvasView.contentInset = UIEdgeInsets(top: insets[1], left: insets[0], bottom: insets[1], right: insets[0])

        }
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension SignatureVC: PKCanvasViewDelegate {

    // Delegate method: Note that the drawing has changed.
    func canvasViewDrawingDidChange(_ canvasView: PKCanvasView) {
        print("canvasViewDrawingDidChange")
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        self.underlayView
    }

    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        switch scrollView {
        case canvasView:
            print(Self.self, #function)
            if self.underlayView.image != nil {
                let offsetX: CGFloat = max((scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5, 0.0)
                let offsetY: CGFloat = max((scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5, 0.0)

                self.underlayView.frame.size = self.addonImage.size * self.canvasView.zoomScale
                self.underlayView.center = CGPoint(x: scrollView.contentSize.width * 0.5 + offsetX, y: scrollView.contentSize.height * 0.5 + offsetY)
            }
            
        default:
            break
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        switch scrollView {
        case canvasView:
            print(Self.self, #function)
        default:
            break
        }
    }

}

extension SignatureVC: PKToolPickerObserver {

    func toolPickerSelectedToolDidChange(_ toolPicker: PKToolPicker) {
        print(Self.self, #function)
    }

    func toolPickerIsRulerActiveDidChange(_ toolPicker: PKToolPicker) {
        print(Self.self, #function)
    }

    func toolPickerVisibilityDidChange(_ toolPicker: PKToolPicker) {
        print(Self.self, #function)
    }

    func toolPickerFramesObscuredDidChange(_ toolPicker: PKToolPicker) {
        print(Self.self, #function)
    }

}




