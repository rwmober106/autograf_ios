//
//  UIManager.swift
//  ThatDubaiGirl
//
//  Created by DevMober on 23.11.20..
//

import Foundation
import UIKit
import JGProgressHUD

class UIManager {
    
    static let shared = UIManager()
    var HUD: JGProgressHUD?
   
    
    //MARK:- JGProgressHUD
      
    func showHUD(view: UIView) {
        self.HUD = JGProgressHUD(style: .extraLight)
        self.HUD?.indicatorView = JGProgressHUDIndeterminateIndicatorView()
        self.HUD?.show(in: view)
    }
    
    func hideHUD() {
        if ( self.HUD != nil ){
            if (self.HUD?.isVisible)! {
                self.HUD?.dismiss()
            }
        }
        
    }
    
    // MARK:- Full Screen Indicator
    
    func showIndicator(vc: UIViewController) {        
        let loadingVC = LoadingViewController()
        loadingVC.modalPresentationStyle = .overFullScreen
        loadingVC.modalTransitionStyle = .crossDissolve
        vc.present(loadingVC, animated: true, completion: nil)
    }
    
    func hideIndicator(vc: UIViewController) {
        vc.dismiss(animated: false, completion: nil)
    }
    
    //MARK:- Alerts
    func showAlert(vc: UIViewController, title: String?, message: String, handler: ((UIAlertAction) -> Swift.Void)? = nil) -> Void {
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAlert = UIAlertAction(title: "OK", style: .default, handler: handler)
        alertVC.addAction(okAlert)
        vc .present(alertVC, animated: true, completion: nil)
    }
    
    func showError(_ error: Error?, for controller: UIViewController) {
        let alert = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        controller.present(alert, animated: true, completion: nil)
    }
    
    func showTx(_ title: String?, txhash: String, with message: String?, for controller: UIViewController, dismissOnCompletion: Bool) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ropstenEtherscanLink = UIAlertAction(title: "Check in Ropsten Etherscan Explorer", style: .default, handler: { (action:UIAlertAction!) -> Void in
            UIApplication.shared.open(URL(string: "\(Web3swiftService.ropstenEtherscanExplorerString)\(txhash)")!, options: [:], completionHandler: nil)
        })
        
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: { _ in
            if dismissOnCompletion {
                controller.dismiss(animated: true, completion: nil)
            }
        })
        alert.addAction(ropstenEtherscanLink)
        alert.addAction(cancelAction)
        controller.present(alert, animated: true, completion: nil)
    }
    
}

