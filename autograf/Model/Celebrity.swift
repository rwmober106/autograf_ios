//
//  Celebrity.swift
//  ThatDubaiGirl
//
//  Created by DevMober on 23.11.20..
//

import Foundation
import ObjectMapper

struct Celebrity: Mappable {
    
    var celebrityId: Int?
    var email: String?
    var name: String?
    var firstName: String?
    var lastName: String?
    var phoneNumber: String?
    var celebrityType: String?
    var cAvatarUrl: String?
    var walletAddress: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        email <- map["email"]
        celebrityId <- map["celebrity_id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        phoneNumber <- map["phone_number"]
        celebrityType <- map["celebrity_type"]
        cAvatarUrl <- map["cavatar_url"]
        walletAddress <- map["cwallet_address"]
        
        name = firstName! + " " + lastName!
    }
    
    
    
    
}
