//
//  Request.swift
//  ThatDubaiGirl
//
//  Created by DevMober on 24.11.20..
//

import Foundation
import ObjectMapper

struct Request: Mappable {
    var requestId: Int?
    var message: String?
    var imageUrl: String?
    var celebrityId: Int?
    var customerId: Int?
    var customerName: String?
    var celebrityName: String?
    var avatarUrl: String?
    var cAvatarUrl: String?
    var status: Int?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        requestId <- map["request_id"]
        message <- map["message"]
        imageUrl <- map["image_url"]
        celebrityId <- map["celebrity_id"]
        customerId <- map["customer_id"]
        customerName <- map["customer_name"]
        celebrityName <- map["celebrity_name"]
        avatarUrl <- map["avatar_url"]
        cAvatarUrl <- map["cavatar_url"]
        status <- map["status"]
    }
    
}
