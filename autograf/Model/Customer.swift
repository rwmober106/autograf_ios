//
//  Customer.swift
//  ThatDubaiGirl
//
//  Created by DevMober on 24.11.20..
//

import Foundation
import ObjectMapper

struct Customer: Mappable {
    var customerId: Int?
    var customerName: String?
    var walletAddress: String?
    var avatarUrl: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        customerId <- map["customer_id"]
        customerName <- map["customer_name"]
        walletAddress <- map["wallet_address"]
        avatarUrl <- map["avatar_url"]
    }
    
}
