//
//  UserType.swift
//  autograf
//
//  Created by DevMober on 24.3.21..
//

import Foundation

enum UserType {
    case athlete
    case customer
}
