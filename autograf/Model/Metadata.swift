//
//  Metadata.swift
//  autograf
//
//  Created by DevMober on 6.4.21..
//

import Foundation
import ObjectMapper

struct Metadata: Mappable {
    var name: String?
    var description: String?
    var image: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        name <- map["name"]
        description <- map["description"]
        image <- map["image"]
        
    }
    
}
