//
//  Vendor.swift
//  ThatDubaiGirl
//
//  Created by DevMober on 24.11.20..
//

import Foundation
import ObjectMapper
import web3swift
import BigInt

struct Autograph: Mappable {
    var id: Int?
    var creator: EthereumAddress?
    var uri: String?
    var price: String?
    var owner: EthereumAddress?
    var isFirstSold: Bool?
    var forSale: Bool?
    var requestId: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id <- map["0"]
        creator <- map["creator"]
        uri <- map["uri"]
        price <- map["price"]
        owner <- map["owner"]
        isFirstSold <- map["isFirstSold"]
        forSale <- map["forSale"]
        requestId <- map["requestId"]
    }
    
}
